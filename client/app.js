import Vue from 'vue'
import { sync } from 'vuex-router-sync'
import Ripple from 'vue-ripple-directive'
import VueFloatLabel from 'vue-float-label'
import VueCordova from 'vue-cordova'
import vueConfig from 'vue-config'
import VueMoment from 'vue-moment'
import { directive as onClickOutside } from 'vue-on-click-outside'
import App from './components/App.vue'
import router from './router'
import store from './store'
// import * as bootstrap from './static/css/bootstrap.css'
import * as css from './static/scss/app.scss'
import * as animated from './static/css/animated.css'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import 'swiper/dist/css/swiper.css'
//import ReadMore from 'vue-read-more'
//import Tab from 'vue-swipe-tab'
import VueTabs from 'vue-nav-tabs'
import 'vue-nav-tabs/themes/vue-tabs.css'
//import {Tabs, Tab} from 'vue-tabs-component';
//Vue.component('tabs', Tabs);
//Vue.component('tab', Tab);
import VueParticles from 'vue-particles'
import VueVideoPlayer from 'vue-video-player'
Vue.use(VueVideoPlayer)

window.$ = window.jQuery = require('jquery');


const configs = {

}

Vue.use(vueConfig, configs)

Vue.use(VueCordova)
Vue.use(VueFloatLabel)
Vue.use(VueMoment)
Vue.directive('ripple', Ripple)
    // Vue.use(bootstrap)
Vue.use(css)
Vue.use(animated)
Vue.directive('on-click-outside', onClickOutside)
    // Vue.use(VueFire);
Vue.use(VueAwesomeSwiper)
    //Vue.use(ReadMore)
    //Vue.use(Tab)
Vue.use(VueTabs)
Vue.use(VueParticles)

sync(store, router)
const app = new Vue({
    router,
    store,
    ...App
})

export { app, router, store }