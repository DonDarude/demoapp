export default {
  "base": "https://myneighby.herokuapp.com/",
  "login": "api/v2/login",
  "modules": "api/v2/features",
  "posts": "api/v2/communities",
  "signUp": "api/v2/signup",
  "users": "api/v2/users"
}