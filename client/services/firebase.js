import Firebase from 'firebase'

var config = {
  apiKey: "AIzaSyDh_iJ9unAGod960qYWztY20gJeq0Gvx28",
  databaseURL: "https://myneighby-d3918.firebaseio.com/",
  projectId: "myneighby-d3918",
  storageBucket: 'gs://myneighby-d3918.appspot.com'
}
const firebaseApp = Firebase.initializeApp(config)
export const db = firebaseApp.database()
export const auth = firebaseApp.auth()
export const storage = firebaseApp.storage()