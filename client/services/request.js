import Vue from 'vue'
import VueResource from 'vue-resource'
import store from '../store'
import service from '.'

Vue.use(VueResource)

export default {
  post(nodeAddress, data, hideLoader) {
    return new Promise((resolve, reject) => {
      let api = service.base + nodeAddress
      var token = this.getUserToken()
      if (!hideLoader) { store.commit("loading", true) }
      Vue.http.post(api, data, {
        headers: { "Content-Type": "application/json", "token": token },
        timeout: 30000
      }).then(res => {
        store.commit("loading", false)
        resolve(res)
      }, err => {
        store.commit("loading", false)
        store.commit("error", err.body)
          // if (err.ok === false) {
          //     this.handleRefreshToken()
          // }
        reject(err)
      })
    })
  },
  patch(nodeAddress, data, hideLoader) {
    return new Promise((resolve, reject) => {
      let api = service.base + nodeAddress
      var token = this.getUserToken()
      if (!hideLoader) { store.commit("loading", true) }
      Vue.http.patch(api, data, {
        headers: { "Content-Type": "application/json", "token": token },
        timeout: 30000
      }).then(res => {
        store.commit("loading", false)
        resolve(res)
      }, err => {
        store.commit("loading", false)
          // if (err.ok === false) {
          //     this.handleRefreshToken()
          // }
        store.commit("error", err.body)
        reject(err)
      })
    })
  },
  get(nodeAddress, data, hideLoader) {
    return new Promise((resolve, reject) => {
      let api = service.base + nodeAddress
      var token = this.getUserToken()
      if (!hideLoader) { store.commit("loading", true) }
      Vue.http.get(api, {
        headers: {
          "token": token
        },
        timeout: 30000
      }).then(res => {
        resolve(res)
        store.commit("loading", false)
      }, err => {
        reject(err)
        store.commit("loading", false)
        store.commit("error", err.body)

        // if (err.ok === false) {
        //     this.handleRefreshToken()
        // }
      })
    })
  },
  delete(nodeAddress, hideLoader) {
    return new Promise((resolve, reject) => {
      let api = service.base + nodeAddress
      var token = this.getUserToken()
      if (!hideLoader) { store.commit("loading", true) }
      Vue.http.delete(api, {
        headers: { "Content-Type": "application/json", "token": token },
        timeout: 30000
      }).then(res => {
        store.commit("loading", false)
        resolve(res)
      }, err => {
        store.commit("loading", false)
        store.commit("error", err.body)

        // if (err.ok === false) {
        //     this.handleRefreshToken()
        // }
        reject(err)
      })
    })
  },
  getUserToken() {
    if (store.state.user && store.state.user.token) {
      return store.state.user.token
    } else {
      store.commit("user", JSON.parse(localStorage.getItem("authData")))
      return store.state.user.token
    }
  }

}