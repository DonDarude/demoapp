import Vue from "vue"
import VueResource from "vue-resource"
import service from '../services'
import request from '../services/request'
import { db, auth } from '../services/firebase'
const R = require('ramda')
import _ from 'lodash'

const updateNode = R.curry((baseNode, strField) => `${baseNode}/${strField}`)
const updateNode2 = R.curry(
  (nodeName, idx, objUpdate) => {
    let strRoute = `/${nodeName}/${idx}`;
    let objTemp = {};
    let arrUpdate = _.each(objUpdate, (value, key) => {
      objTemp[strRoute + `/${key}`] = value
    })
    return objTemp
  });

Vue.use(VueResource)

const http = Vue.http
export default {
  register({ commit, dispatch }, payload) {
    commit('loading', true)
    return new Promise((resolve, reject) => {
      auth.createUserWithEmailAndPassword(payload.email, payload.password)
        .then(res => {
          auth.signInWithEmailAndPassword(payload.email, payload.password)
            .then(res => {
              dispatch('updateProfile', {
                displayName: payload.displayName,
                photoURL: payload.photoURL
              }).then(res => {
                commit('loading', false)
                resolve(res)
              })
            })
            .catch((err) => {
              commit('loading', false)
              commit('error', err)
              reject(err)
            })

        }, (err) => {
          commit('loading', false)
          commit('error', err)
          reject(err)
        })
    })
  },
  updateProfile({ commit }, payload) {
    return new Promise((resolve, reject) => {
      var user = auth.currentUser;
      user.updateProfile({
        displayName: payload.displayName,
        photoURL: payload.photoURL
      }).then(function() {
        resolve(true)
      }).catch(function(error) {
        reject(error)
      });
    })
  },
  login({ commit }, payload) {
    commit('loading', true)
    return new Promise((resolve, reject) => {
      auth.signInWithEmailAndPassword(
        payload.username,
        payload.password
      ).then(res => {
        resolve(res)
        commit('loading', false)
      }, err => {
        commit('error', err)
        reject(err)
        commit('loading', false)
      })
    })
  },
  newPost({ commit }, payload) {
    return new Promise((resolve, reject) => {
      let data = payload
      let ref = db.ref(payload.content.community.id + "/posts/")
      ref.push(data).then(res => {
        let newPost = {
          [res.key]: {
            ...data
          }
        }
        commit('newPost', newPost)
        resolve(res)
      })

    })
  },
  editPost({ commit }, payload) {
    return new Promise((resolve, reject) => {
      let data = payload
      let ref = db.ref(payload.content.community.id + "/posts/" + data.content.key + "/content")

      ref.set(data.content, res => {
        resolve(res)
      })
    })
  },
  getFeatures({ commit }) {
    return new Promise((resolve, reject) => {
      request.get(service.modules).then(response => {
        commit("modules", response.body.modules)
        localStorage.setItem('features', JSON.stringify(response.body.modules))
        resolve(true)
      }, (err) => {
        commit("modules", null)
        reject(err)
      })
    })
  },
  getPosts({ commit }, payload) {
    if (payload.all && payload.reload) {
      return new Promise((resolve, reject) => {
        request.get(service.posts + '/' + payload.currentCommunity + '/posts').then(response => {
          var r = response.body.posts.map((p, index) => {
            if (index < 3) {
              p.show = true
            }
            return p
          })
          commit("posts", r)
          commit("postType", 'all')
          resolve(true)
        }, (err) => {
          commit("posts", null)
          reject(err)
        })
      })
    } else if (payload.your && payload.reload) {
      return new Promise((resolve, reject) => {
        request.get(service.posts + '/' + payload.currentCommunity + '/posts/mine').then(response => {
          var r = response.body.posts.map((p, index) => {
            if (index < 3) {
              p.show = true
            }
            return p
          })
          commit("posts", r)
          commit("postType", 'your')
          resolve(true)
        }, (err) => {
          commit("posts", null)
          reject(err)
        })
      })
    }

  },
  getUser({ commit }, payload) {
    return new Promise((resolve, reject) => {
      let api = service.users + "/" + payload.id
      request.get(api).then(response => {
        commit("selectedUser", response.body.user)
        resolve(true)
      }, err => {
        commit("selectedUser", null)
        reject(err)
      })
    })
  },
  updateFirebaseUser({ commit }, payload) {
    return new Promise((resolve, reject) => {

      const ref = db.ref(payload.community.id)
      const node = `${'/users/'}${payload.user.uid}`
      const data = {
        [updateNode(node)('displayName')]: payload.user.displayName,
        [updateNode(node)('photoURL')]: payload.user.photoURL,
        [updateNode(node)('email')]: payload.user.email
      }
      ref.update(data).then(res => {
        resolve(res)
      })
    })
  },
  updateFirebase({ commit }, payload) {
    return new Promise((resolve, reject) => {
      let objUpdate = {
        'displayName': payload.user.displayName,
        'photoURL': payload.user.photoURL,
        'email': payload.user.email
      };
      const ref = db.ref(payload.community.id)
      ref.update(updateNode2('users', payload.user.uid, objUpdate)).then(res => {
        resolve(res)
      })
    })
  }
}