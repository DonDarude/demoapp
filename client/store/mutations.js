import _ from 'lodash'

export default {
  showTab(state, payload) {
    state.showTab = payload
  },
  cordova(state, payload) {
    state.cordova = payload
  },
  communities(state, payload) {
    state.communities = payload
  },
  user(state, payload) {
    state.user = payload
  },
  error(state, payload) {
    state.error = payload
  },
  loading(state, payload) {
    state.loading = payload
  },
  modules(state, payload) {
    state.modules = payload
  },
  posts(state, payload) {
    state.posts = payload
      // let arrPost = _.map(payload, (obj, key) => {
      //   obj.key = key
      //   state.lastPostKey = key
      //   return obj
      // })
      // let arrSorted = _.orderBy(arrPost, [(obj) => obj.content.postedDate], ['desc'])
      // state.posts = arrSorted
  },
  editPost(state, payload) {
    state.posts = state.posts.map(o => {
      if (payload.key === o.key) {
        return payload
      } else {
        return o
      }
    })
  },
  selectedPost(state, payload) {
    state.selectedPost = payload
  },
  postType(state, payload) {
    state.postType = payload
  },
  updatePost(state, payload) {
    const post = {
      [payload.key]: {
        ...payload.value
      }
    }
    state.posts = {...state.posts, ...post }
  },
  updatePostLike(state, payload) {
    state.posts[payload.key].likedBy = payload.likedBy
  },
  updatePostComment(state, payload) {
    let posts = state.posts.map((post) => {
      if (post._id === payload.post._id) {
        post.comments = payload.post.comments
        post.showMore = payload.showMore
        return post
      } else {
        return post
      }
    })
    state.posts = posts
  },
  deletePost(state, payload) {
    let posts = _.filter(state.posts, post => post.key !== payload)
    state.posts = posts
  },
  modal(state, payload) {
    state.modal = payload
  },
  newPost(state, payload) {
    state.posts = {...state.posts, ...payload }
  },
  frontUser(state, payload) {
    state.frontUser = payload
  },
  selectedUser(state, payload) {
    state.selectedUser = payload
  },
  openSideMenu(state, payload) {
    state.loading = false
    state.openSideMenu = payload
  },
  lastPost(state, payload) {
    state.lastPost = payload
  },
  toggleLike(state, payload) {
    let user = state.user
    let uid = user.uid
    state.posts = state.posts.map(o => {
      if (o.key === payload.key) {
        if (o.likedBy && o.likedBy[uid]) {
          o.likedBy = _.remove(l => {
            l === user.uid
          })
        } else {
          o.likedBy = {...o.likedBy,
            [uid]: {
              uid: uid,
              displayName: user.displayName,
              photoURL: user.photoURL
            }
          }
        }
      }
      return o
    })
  },
  addComment(state, payload) {
    let user = state.user
    let uid = user.uid
    let { selectedPost, commentKey } = payload
    state.posts = state.posts.map(o => {
      if (o.key === selectedPost) {
        o.comments = {...o.comments,
          [commentKey]: {
            uid: uid
          }
        }
      }
      return o
    })
  },
  removeComment(state, payload) {
    let selectedPost = payload.selectedPost
    let selectedCommentKey = payload.selectedCommentKey
    state.post = state.posts.map(o => {
      if (o.key === selectedPost) {
        delete o.comments[selectedCommentKey]
      }
      return o
    })
  }
}