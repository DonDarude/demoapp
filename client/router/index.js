import Vue from 'vue'
import Router from 'vue-router'
import Landing from '../views/Landing'
import Book from '../views/book'
import Testing from '../views/Testing'
import Testing2 from '../views/Testing2'

Vue.use(Router)
export default new Router({
    mode: 'hash',
    base: __dirname,
    routes: [{
            name: 'landing',
            path: '/',
            component: Landing
        },
        {
            name: 'book',
            path: '/book/',
            component: Book
        },
        {
            name: 'testing',
            path: '/testing/',
            component: Testing
        },
        {
            name: 'testing2',
            path: '/testing2/',
            component: Testing2
        }
    ]
})